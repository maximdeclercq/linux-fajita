PMBOOTSTRAP_CFG = $(PWD)/pmbootstrap.cfg
PMBOOTSTRAP_TMP = $(PWD)/pmbootstrap.tmp
PMBOOTSTRAP_TMP_APORTS = $(PMBOOTSTRAP_TMP)/cache_git/pmaports
PMBOOTSTRAP = pmbootstrap \
			  --config="$(PMBOOTSTRAP_CFG)" \
			  --work="$(PMBOOTSTRAP_TMP)" \
			  --aports="$(PMBOOTSTRAP_TMP_APORTS)"

all: install
	
$(PMBOOTSTRAP_TMP_APORTS):
	mkdir --parents "$(PMBOOTSTRAP_TMP_APORTS)"

config: $(PMBOOTSTRAP_TMP_APORTS)
	$(PMBOOTSTRAP) init

install:
	$(PMBOOTSTRAP) install --fde

flash:
	fastboot set_active a
	fastboot erase dtbo
	$(PMBOOTSTRAP) flasher flash_rootfs --partition userdata
	$(PMBOOTSTRAP) flasher flash_kernel
